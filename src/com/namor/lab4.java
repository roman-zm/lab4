package com.namor;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class lab4 {
    public static class Matrix {
        private int[][] matrix;
        private int[] cursor;
        private int weight = -1;

        public Matrix(int x, int y) throws IllegalArgumentException {
            if (between(x) && between(y)) {
                matrix = getMatrix();
                cursor = new int[2];
                cursor[0] = x;
                cursor[1] = y;
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static Matrix genRandomMatrix() {
            int[][] mat = getMatrix();
            ArrayList<Integer> rand = getRandom();
            int x = 0;
            int y = 0;
            int index = 0;

            for (int i = 0; i < mat.length; i++) {
                for (int j = 0; j < mat[i].length; j++) {
                    mat[i][j] = rand.get(index);
                    if (mat[i][j] == 0) {
                        x = i;
                        y = j;
                    }
                    index++;
                }
            }

            Matrix matr = new Matrix(x, y);
            matr.matrix = mat;
            return matr;
        }

        public static Matrix genSolvableMatrix() {
            Matrix matrix = new Matrix(1, 3);
            matrix.matrix = new int[][]{
                    new int[] {1, 2, 3, 4},
                    new int[] {5, 6, 7, 0},
                    new int[] {9, 10, 11, 8},
                    new int[] {13, 14, 15, 12},
            };

//            int to = new Random().nextInt(25) + 10;
            int to = new Random().nextInt(25) + 10;

            for (int i = 0; i < to; i++) {
                int bound = Direction.values().length;
                try {
                    matrix = matrix.move(Direction.values()[new Random().nextInt(bound)]);
                } catch (IllegalStateException ignored) {}
            }

            return matrix;
        }

        public Matrix move(Direction direction) throws IllegalStateException {
            int[][] resp = clone2d(matrix);
            int x = cursor[0];
            int y = cursor[1];

            int mx = x;
            int my = y;

            switch (direction) {
                case Right:
                    if (between(y + 1)) my = y + 1;
                    else throw new IllegalStateException();
                    break;
                case Left:
                    if (between(y - 1)) my = y - 1;
                    else throw new IllegalStateException();
                    break;
                case Up:
                    if (between(x - 1)) mx = x - 1;
                    else throw new IllegalStateException();
                    break;
                case Down:
                    if (between(x + 1)) mx = x + 1;
                    else throw new IllegalStateException();
                    break;
                default:
                    throw new IllegalStateException();
            }

            resp = replace(x, y, mx, my, resp);
            Matrix response = new Matrix(mx, my);
            response.matrix = resp;

            return response;
        }

        private static int[][] replace(int x, int y, int mx, int my, int[][] input) {
            int buffer = input[mx][my];
            input[x][y] = buffer;
            input[mx][my] = 0;
            return input;
        }

        public int[][] getArray() {
            return matrix;
        }

        private static int[][] getMatrix() {
            return new int[4][4];
        }

        private static boolean between(int in) {
            return between(in, 0, 3);
        }

        public void print() {
            for (int[] line : matrix) {
                System.out.println(Arrays.toString(line));
            }
        }

        public String getString() {
            StringBuilder builder = new StringBuilder();
            for (int[] line : matrix) {
                builder.append(Arrays.toString(line)).append("\n");
            }

            return builder.toString();
        }

        private static boolean between(int in, int from, int to) {
            return in >= from && in <= to;
        }

        public enum Direction {
            Up,
            Down,
            Left,
            Right
        }

        private static int[][] clone2d(int[][] source) {
            int[][] copy = getMatrix();
            for (int i = 0; i < source.length; i++) {
                copy[i] = source[i].clone();
            }
            return copy;
        }

        private static ArrayList<Integer> getRandom() {
            return ThreadLocalRandom.current()
                    .ints(0, 16)
                    .distinct()
                    .limit(16)
                    .boxed()
                    .collect(Collectors.toCollection(ArrayList::new));
        }

        public static boolean check(Matrix mat) {
            int[][] matrix = mat.matrix;
            int next = 1;
            for (int[] line : matrix) {
                for (int number : line) {
                    if (number == next) {
                        next++;
                    } else if (number != 0) {
                        return false;
                    }
                }
            }
            if (next == 16) return true;
            return false;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Matrix matrix1 = (Matrix) o;

            return Arrays.deepEquals(matrix, matrix1.matrix);
        }

        @Override
        public int hashCode() {
            return Arrays.deepHashCode(matrix);
        }

        public int getWeight() {
            if (weight != -1) return weight;
            int next = 1;
            for (int[] line : matrix) {
                for (int number : line) {
                    if (number == next) {
                        next++;
                    } else if (number != 0) {
                        return next;
                    }
                }
            }
            weight = next;
            return next;
        }
    }

    public static void main(String[] args) {
        Matrix matrix = Matrix.genSolvableMatrix();

        matrix.print();
        System.out.println(Arrays.toString(matrix.cursor));

//        Map<Matrix, List<Matrix>> nodes = new HashMap<>();
//        nodes.put(matrix, getChilds(matrix));

        search(matrix, Matrix::check);
    }

    private static void search(Matrix initialMatrix, Predicate<Matrix> predicate) {

        Map<Matrix, ParentAction> parents = new HashMap<>();

        Map<Matrix, List<Matrix>> nodes = new HashMap<>();
        Set<Matrix> generated = new HashSet<>();
        generated.add(initialMatrix);

        LinkedList<Matrix> open = new LinkedList<>();
        open.add(initialMatrix);

        LinkedList<Matrix> closed = new LinkedList<>();
        int step = 1;

        while (!open.isEmpty()) {
            Matrix currentNode = open.poll();
            closed.add(currentNode);

            if (currentNode.getWeight() == 16) {
                System.out.println("Result :");
                System.out.println(step);
                currentNode.print();

                writePath(parents, currentNode);

                return;
            }

            nodes.put(currentNode, getChilds(currentNode, generated, parents));

            nodes.get(currentNode).stream()
                    .filter(matrix -> !open.contains(matrix))
                    .filter(matrix -> !closed.contains(matrix))
//                    .peek(matrix -> {matrix.print(); System.out.println();})
                    .forEachOrdered(open::addLast);
            step++;
        }
    }

    private static void writePath(Map<Matrix, ParentAction> parents, Matrix input) {
        Matrix current = input;

        LinkedList<String> path = new LinkedList<>();
//        LinkedHashMap<Matrix, ParentAction> path = new LinkedHashMap<>();

        System.out.println("\n -----------------path---------------- \n");

        path.addFirst(current.getString());
        while (parents.containsKey(current)) {
            ParentAction parentAction = parents.get(current);
            path.addFirst(parentAction.operator.name() + "\n");
            path.addFirst(parentAction.matrix.getString());
            current = parentAction.matrix;
        }

        path.stream().forEachOrdered(System.out::println);

    }

    private static List<Matrix> getChilds(Matrix matrix, Set<Matrix> generated, Map<Matrix, ParentAction> parents) {
        List<Matrix> childs = new ArrayList<>();

        for (Matrix.Direction direction : Matrix.Direction.values()) {
            try {
                Matrix child = matrix.move(direction);
                if (!generated.contains(child))
                    parents.put(child, new ParentAction(matrix, direction));
                childs.add(child);
            } catch (IllegalStateException e) {}
        }

        return childs.stream()
                .filter(generated::add)
                .collect(Collectors.toCollection(ArrayList::new));

    }


    private static class ParentAction {
        private Matrix matrix;
        private Matrix.Direction operator;

        public ParentAction(Matrix matrix, Matrix.Direction operator) {
            this.matrix = matrix;
            this.operator = operator;
        }
    }
}
