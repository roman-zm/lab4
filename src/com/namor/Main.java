package com.namor;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.IntConsumer;
import java.util.function.Supplier;

public class Main {

    public enum SearchType {
        Breadth,
        Depth
    }

    public static void main(String[] args) throws IOException {
        File file = new File("graph.txt");

        Scanner in = new Scanner(file);
        int nodesCount = Integer.parseInt(in.nextLine());
        int[][] nodes = new int[nodesCount][];
        for (int i = 0; i < nodesCount; i++) {
            String str = in.nextLine();
            System.out.println(str);
            int node = Integer.parseInt(str.split(" ")[0]);

            nodes[node] = Arrays.stream(str.split(" "))
                    .mapToInt(Integer::parseInt)
                    .skip(1)
                    .toArray();
        }
        System.out.println("nodes = " + Arrays.deepToString(nodes));

        System.out.println("Breadth");
        System.out.println(search(nodes, 7, SearchType.Breadth));
        System.out.println("Depth");
        System.out.println(search(nodes, 7, SearchType.Depth));
        LinkedList<Integer> path = new LinkedList<>();
//        System.out.println(RecursiveDepthSearch(nodes, 0, 4, null, path, null));
//        System.out.println("path = " + path);
    }

    public static String search(int[][] nodes, int goal, SearchType searchType) throws IllegalArgumentException {

        int steps = 0;

        Map<Integer, Integer> parents = new HashMap<>();

        LinkedList<Integer> open = new LinkedList<>();
        open.add(0);

        IntConsumer openInserter = getListInserter(open, searchType);

        if (openInserter == null)
            throw new IllegalArgumentException("Unknown search type");

        LinkedList<Integer> closed = new LinkedList<>();

        while (!open.isEmpty()) {
            steps++;

            int currentNode = open.poll();
            closed.add(currentNode);

            if (currentNode == goal) {
                System.out.println("steps = " + steps);
                return getPath(0, goal, parents);
            }

            List<Integer> buffer = new LinkedList<>();
            LinkedList<Integer> finalOpen = open;
            Arrays.stream(nodes[currentNode])
                    .filter(value -> !finalOpen.contains(value))
                    .filter(value -> !closed.contains(value))
                    .peek(child -> parents.put(child, currentNode))
                    .forEachOrdered(buffer::add);

            open = merge(open, buffer, searchType);

        }
        return "";
    }

    private static LinkedList<Integer> merge(LinkedList<Integer> open, List<Integer> buffer, SearchType searchType) {
        LinkedList<Integer> response = new LinkedList<>();
        if (searchType == SearchType.Breadth) {
            response.addAll(buffer);
            response.addAll(open);
        } else {
            response.addAll(open);
            response.addAll(buffer);
        }
        return response;
    }

    private static String getPath(int from, int to, Map<Integer, Integer> parents) {
        int current = to;
        String path = "";
        while (current != from) {
            path = " => " + current + path;
            current = parents.get(current);
        }
        path = from + path;
        return path;
    }

    public static IntConsumer getListInserter(LinkedList<Integer> list ,SearchType type) {
        switch (type) {
            case Depth:
                return list::addFirst;
            case Breadth:
                return list::addLast;
            default:
                return null;
        }
    }

    public interface Incrementer {
        void increment();
        int getValue();
    }

    public static boolean RecursiveDepthSearch(
            int[][] nodes,
            int current,
            int goal,
            LinkedList<Integer> inClosed,
            LinkedList<Integer> inPath,
            Incrementer incrementer
    ) {
        boolean printSteps = incrementer == null;
        int steps;
        if (printSteps) {
            steps = 1;
            incrementer = new Incrementer() {
                private int x = 1;

                @Override
                public void increment() {
                    x++;
                }

                @Override
                public int getValue() {
                    return x;
                }


            };
        }
        incrementer.increment();

        LinkedList<Integer> closed = inClosed != null ? inClosed : new LinkedList<Integer>();
        LinkedList<Integer> path = inPath != null ? inPath : new LinkedList<Integer>();

        if (current == goal) {
            path.addFirst(current);
            return true;
        }
        closed.add(current);

        for (int node : nodes[current]) {
            if (!closed.contains(node) && RecursiveDepthSearch(nodes, node, goal, closed, path, incrementer)) {
                path.addFirst(current);
                if (printSteps) System.out.println("steps = " + incrementer.getValue());
                return true;
            }
        }
        return false;
    }
}
